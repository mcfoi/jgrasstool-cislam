=======================================================================
====================  THIS REPOSITORY IS OUTDATED =====================
=======================================================================

Due to the migration of the forked repository JGrassTools from Google
Code to GitHub, also this fork was moved from BitBucket to GitHub to
allow pull requests into the main branch.

=======================================================================
Please, find the most updated version of CISLAM Model in:

https://github.com/mcfoi/jgrasstools


Marco Foi