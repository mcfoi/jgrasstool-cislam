This package represents the delivery of the GSoC 2013 initiative developed by Marco Foi and having the following title:
"Hydrologically controlled shallow landslides - a statistical forecasting algorithm for the uDig Spatial Toolbox"

Specifically, the whole JGrassTools framework has been provided despite the actual developed code is contained in the folder:
./hortonmachine/src/main/java/org/jgrasstools/hortonmachine/modules/hydrogeomorphology/cislam
This has been done to allow compiling of the whole Maven project into a usable JAR.

The developed code adds to the framework one module for computing shallow landslides susceptibility analysis on a basin: this model has been named CI-Slam and is based on the concepts developed in the following notable scientific paper:
Lanni, C., Borga, M., Rigon, R. and Tarolli, P. (2012) Modelling shallow landslide susceptibility by means of a subsurface flow path connectivity index and estimates of soil depth spatial distribution. Hydrology and Earth System Sciences, 16(11), pp. 3959-3971. 
Available at: http://www.hydrol-earth-syst-sci.net/16/3959/2012/hess-16-3959-2012.html

To compile the Maven 3.x project he code into a usable JAR please follow instruction available here:
https://code.google.com/p/jgrasstools/wiki/JARExporting
then use the Tutorial to plug the module package into the uDig's Spatial Toolbox:
http://www.mcfoi.it/msccs/thesis/OmsCislam.html

Marco Foi
 