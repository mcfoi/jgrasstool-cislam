/*
 * This file is part of the "CI-slam module": an addition to JGrassTools
 * It has been entirely contributed by Marco Foi (www.mcfoi.it)
 * 
 * "CI-slam module" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

// TODO Resort to using default framework class HortonMessageHandler and default bundle
/*
 * NOTE TAHT THIS CLASS HAS TO BE REPLACED WITH THE ORIGINAL:
 *         org.jgrasstools.hortonmachine.i18n.HortonMessageHandler;
 *     before pulling into JGrassTools main repository
 *     AND
 *         src/main/resources/hm_mesagesfoi
 *     HAS TO BE MERGES INTO
 *         src/main/resources/hm_mesages
*/
public class HortonMessageHandlerCislam {

    private static HortonMessageHandlerCislam messageHandler = null;
   
    private Locale locale = Locale.getDefault();

    private ResourceBundle resourceBundle;

    private HortonMessageHandlerCislam() {
    	
    	String user_lang = System.getProperty("user.language");
    	
    	if (user_lang.equalsIgnoreCase("it")){
    		locale = new Locale("it", "IT");
    	}
    }

    public synchronized static HortonMessageHandlerCislam getInstance() {
        if (messageHandler == null) {
            messageHandler = new HortonMessageHandlerCislam();
            messageHandler.initResourceBundle();

        }
        return messageHandler;
    }

    private void initResourceBundle() {
        resourceBundle = ResourceBundle.getBundle("hm_messages", locale);
    }

    public String message( String key ) throws Exception {
        return resourceBundle.getString(key);
    }
}
