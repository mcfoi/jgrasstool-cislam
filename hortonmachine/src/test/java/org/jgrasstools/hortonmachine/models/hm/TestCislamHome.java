/*
 * This file is part of the "CI-slam module": an addition to JGrassTools
 * It has been entirely contributed by Marco Foi (www.mcfoi.it)
 * 
 * "CI-slam module" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.jgrasstools.hortonmachine.models.hm;

import java.util.ArrayList;
import java.util.List;

import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.jgrasstools.gears.io.rasterreader.OmsRasterReader;
import org.jgrasstools.gears.io.rasterwriter.OmsRasterWriter;
import org.jgrasstools.gears.libs.exceptions.ModelsIllegalargumentException;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.OmsCislam;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsPsiInitAtBedrock;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsSafetyFactorGeomechanic;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsSafetyFactorsWorstCaseComposer;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsSoilThickness;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsV0;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsVanGenuchtenMapsGenerator;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utility_models.OmsVwt;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utils.MapPreprocessingUtilities;
import org.jgrasstools.hortonmachine.modules.hydrogeomorphology.cislam.utils.ParameterCalculationFunctions;
import org.jgrasstools.hortonmachine.utils.HMTestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test the {@link OmsCislam} module.
 * 
 * @author Marco Foi (www.mcfoi.it)
 */
public class TestCislamHome extends HMTestCase {

    public static final String inputFolder = System.getenv("DROPBOX_ROOT") + "MSc_docs\\7COM0177_-_MSc_Project\\Data\\";// "D:\\Dropbox\\MSc_docs\\7COM0177_-_MSc_Project\\Data\\";
    //public static final String outputFolder = System.getProperty("user.home") + "\\Desktop\\OUTPUT\\";
    public static final String outputFolder = "E:\\cislam-data\\output\\";// For use at home on Sate PC

    @Before
    public void setUp() {

    }

    @Ignore
    public void _cislam() throws Exception {

        // OmsCislam cislam = new OmsCislam();

        // cislam.process();

    }


    public void _test_flowMapRebuildBorder() throws Exception {

        pm.message("##############################################");
        pm.message("### Test test_flowMapRebuildBorder STARTED ###");
        pm.message("##############################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inFlow_filepath = inputFolder + "aa_flow.asc";
        String inPit_filepath = inputFolder + "aa_pit.asc";
        String outFlowBordered_filepath = outputFolder + "aa_flow_bordered.asc";

        GridCoverage2D inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        GridCoverage2D inPit = OmsRasterReader.readRaster(inPit_filepath);
        GridCoverage2D outPit = MapPreprocessingUtilities.flowMapRebuildBorder(inFlow, inPit, pm);
        OmsRasterWriter.writeRaster(outFlowBordered_filepath, outPit);

    }


    public void _test_flowMapRebuildBorder_CislamData() throws Exception {

        pm.message("#########################################################");
        pm.message("### Test test_flowMapRebuildBorder_CislamData STARTED ###");
        pm.message("#########################################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inFlow_filepath = outputFolder + "basin_omsflow.asc"; // Flow
                                                                             // Map
                                                                             // coming
                                                                             // form
                                                                             // OmsFlowDirections
                                                                             // model
        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String outFlowBordered_filepath = outputFolder + "basin_omsflow_bordered.asc";

        GridCoverage2D inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        GridCoverage2D inPit = OmsRasterReader.readRaster(inPit_filepath);
        GridCoverage2D outPit = MapPreprocessingUtilities.flowMapRebuildBorder(inFlow, inPit, pm);
        OmsRasterWriter.writeRaster(outFlowBordered_filepath, outPit);

    }


    public void _test_markFlowOutlet() throws Exception {

        pm.message("########################################");
        pm.message("### Test test_markFlowOutlet STARTED ###");
        pm.message("########################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inFlow_filepath = inputFolder + "aa_flow.asc";
        String outFlowMarked_filepath = outputFolder + "aa_flow_marked.asc";

        // OmsCislam cislam = new OmsCislam();
        // cislam.inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        // cislam.process();

        GridCoverage2D inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        GridCoverage2D outRaster = MapPreprocessingUtilities.flowMapMarkOutlet(inFlow, pm);
        OmsRasterWriter.writeRaster(outFlowMarked_filepath, outRaster);

    }

    @Before
    /** Required  for running any OmsCilsam Test */
    public void _test_fixZeroValuesAndBordersInSlope() throws Exception {

        pm.message("########################################################");
        pm.message("### Test test_fixZeroValuesAndBordersInSlope STARTED ###");
        pm.message("########################################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        // String inPit_filepath = base + "aa_pit.asc";
        // String inSlope_filepath = base + "aa_slope.asc";
        // String outSlopeZeroFixed_filepath = base + output
        // + "aa_slope_zero_and_border_fixed.asc";

        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String inSlope_filepath = inputFolder + "basin_slope.asc";
        String outSlopeZeroFixed_filepath = outputFolder + "basin_slope_zero_and_border_fixed.asc";

        // OmsCislam cislam = new OmsCislam();
        // cislam.inSlope = OmsRasterReader.readRaster(inSlope_filepath);
        // cislam.process();

        GridCoverage2D inPit = OmsRasterReader.readRaster(inPit_filepath);
        GridCoverage2D inSlope = OmsRasterReader.readRaster(inSlope_filepath);
        GridCoverage2D outRaster = MapPreprocessingUtilities.slopeMapFixZeroValuesAndBorder(inSlope, inPit, OmsCislam.MINIMM_ALLOWED_SLOPE,
                pm);
        OmsRasterWriter.writeRaster(outSlopeZeroFixed_filepath, outRaster);

    }

    @Before
    /** Required  for running any OmsCilsam Test */
    public void test_soilThickness() throws Exception {

        pm.message("#######################################");
        pm.message("### Test test_soilThickness STARTED ###");
        pm.message("#######################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inFlow_filepath = outputFolder + "basin_slope_zero_and_border_fixed.asc";
        // inFlow_filepath = base + "basin_slope.asc";
        String outSoilthickness_filepath = outputFolder + "basin_soilthickness.asc";

        OmsSoilThickness soilthikness = new OmsSoilThickness();

        soilthikness.inSlope = OmsRasterReader.readRaster(inFlow_filepath);

        soilthikness.process();

        GridCoverage2D outSoilThickness = soilthikness.outSoilThickness;
        OmsRasterWriter.writeRaster(outSoilthickness_filepath, outSoilThickness);

    }


    public void _test_IntensityDurationFreqencyGumbel() {

        double i = ParameterCalculationFunctions.calculateRainfallIntensity(pm);
        double isupp = 0.021821932078581942;
        boolean result = org.jgrasstools.gears.utils.math.NumericsUtilities.dEq(isupp, i, 0.000000000005);

        assertTrue(result);

    }

    @Before
    /** Required  for running any OmsCilsam Test */
    public void _test_VanGenuchtenMapsGenerator() throws Exception {

        pm.message("###################################################");
        pm.message("### Test test_VanGenuchtenMapsGenerator STARTED ###");
        pm.message("###################################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inGeo_filepath = inputFolder + "basin_geo.asc";

        String outCohesion_filepath = outputFolder + "basin_cohesion.asc";
        String outPhi_filepath = outputFolder + "basin_phi.asc";
        String outGamma_filepath = outputFolder + "basin_gamma_soil.asc";
        String outKsat_filepath = outputFolder + "basin_ksat.asc";
        String outTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String outTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String outAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String outNVanGen_filepath = outputFolder + "basin_n.asc";

        OmsVanGenuchtenMapsGenerator mapGenerator = new OmsVanGenuchtenMapsGenerator();

        mapGenerator.inGeo = OmsRasterReader.readRaster(inGeo_filepath);

        mapGenerator.process();

        GridCoverage2D outCohesion = mapGenerator.outCohesion;
        OmsRasterWriter.writeRaster(outCohesion_filepath, outCohesion);

        GridCoverage2D outPhi = mapGenerator.outPhi;
        OmsRasterWriter.writeRaster(outPhi_filepath, outPhi);

        GridCoverage2D outGamma = mapGenerator.outGamma;
        OmsRasterWriter.writeRaster(outGamma_filepath, outGamma);

        GridCoverage2D outKsat = mapGenerator.outKsat;
        OmsRasterWriter.writeRaster(outKsat_filepath, outKsat);

        GridCoverage2D outTheta_s = mapGenerator.outTheta_s;
        OmsRasterWriter.writeRaster(outTheta_s_filepath, outTheta_s);

        GridCoverage2D outTheta_r = mapGenerator.outTheta_r;
        OmsRasterWriter.writeRaster(outTheta_r_filepath, outTheta_r);

        GridCoverage2D outAlfaVanGen = mapGenerator.outAlfaVanGen;
        OmsRasterWriter.writeRaster(outAlfaVanGen_filepath, outAlfaVanGen);

        GridCoverage2D outNVanGen = mapGenerator.outNVanGen;
        OmsRasterWriter.writeRaster(outNVanGen_filepath, outNVanGen);

    }


    public void _test_safetyFactorGeoTechnical() throws Exception {

        pm.message("");
        pm.message("##################################################");
        pm.message("### Test test_safetyFactorGeoTechnical STARTED ###");
        pm.message("##################################################");

        // String base = "E:\\CRISTIANO\\SILVIA_CADINI\\MODELLO_FRANE\\";
        // String output = "OUTPUT\\";
        String inSlope_filepath = inputFolder + "basin_slope.asc";
        String inPhi_filepath = outputFolder + "basin_phi.asc";
        String inCohesion_filepath = outputFolder + "basin_cohesion.asc";
        String inGammaSoil_filepath = outputFolder + "basin_gamma_soil.asc";
        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";

        String outSafetyFactorGeoTechnical_filepath = outputFolder + "basin_safety_factor_geotech.asc";
      
        OmsSafetyFactorGeomechanic model = new OmsSafetyFactorGeomechanic();
        
        model.inSlope = OmsRasterReader.readRaster(inSlope_filepath);
        model.inPhi = OmsRasterReader.readRaster(inPhi_filepath);
        model.inCohesion = OmsRasterReader.readRaster(inCohesion_filepath);
        model.inGammaSoil = OmsRasterReader.readRaster(inGammaSoil_filepath);
        model.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        
        model.process();
        
        GridCoverage2D outSafetyFactorGeoMechanical = model.outSafetyactorGeoMechanic;

        OmsRasterWriter.writeRaster(outSafetyFactorGeoTechnical_filepath, outSafetyFactorGeoMechanical);
    }


    public void _test_VwtMapGenerator() throws Exception {

        pm.message("");
        pm.message("#########################################");
        pm.message("### Test test_VwtMapGenerator STARTED ###");
        pm.message("#########################################");

        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        String inTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String inTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String inAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String inNVanGen_filepath = outputFolder + "basin_n.asc";

        String outVwt_filepath = outputFolder + "basin_Vwt.asc";

        OmsVwt mapGenerator = new OmsVwt();

        mapGenerator.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        mapGenerator.inTheta_s = OmsRasterReader.readRaster(inTheta_s_filepath);
        mapGenerator.inTheta_r = OmsRasterReader.readRaster(inTheta_r_filepath);
        mapGenerator.inAlfaVanGen = OmsRasterReader.readRaster(inAlfaVanGen_filepath);
        mapGenerator.inNVanGen = OmsRasterReader.readRaster(inNVanGen_filepath);

        mapGenerator.process();

        GridCoverage2D outVwt = mapGenerator.outVwt;
        OmsRasterWriter.writeRaster(outVwt_filepath, outVwt);
    }

    @Before
    /** Required  for running any OmsCilsam Test */
    public void _test_OmsPsiInitAtBedrock() throws Exception {

        pm.message("");
        pm.message("#############################################");
        pm.message("### Test test_OmsPsiInitAtBedrock STARTED ###");
        pm.message("#############################################");

        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        double mockPsiValue = 0.15;

        String outPsiInitAtBedrock_filepath1 = outputFolder + "basin_PsiInitAtBedrock_soil-based.asc";
        String outPsiInitAtBedrock_filepath2 = outputFolder + "basin_PsiInitAtBedrock_const-based.asc";
        String outPsiInitAtBedrock_filepath3 = outputFolder + "basin_PsiInitAtBedrock_param-based.asc";

        OmsPsiInitAtBedrock mapGenerator;

        // Test 1 -> MUST THROW ModelsIllegalargumentException
        try {
            mapGenerator = new OmsPsiInitAtBedrock();
            mapGenerator.process();
        } catch (ModelsIllegalargumentException e) {
            pm.errorMessage("Either Pit or Soil Thickness are required");
        }

        // Test 2 -> MUST produce soil based map
        mapGenerator = new OmsPsiInitAtBedrock();
        mapGenerator.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        mapGenerator.process();

        GridCoverage2D outPsiInitAtBedrock1 = mapGenerator.outPsiInitAtBedrock;
        OmsRasterWriter.writeRaster(outPsiInitAtBedrock_filepath1, outPsiInitAtBedrock1);

        // Test 3 -> MUST produce a map based on DEFAULT_PSI_CONSTANT (=0.05m)
        mapGenerator = new OmsPsiInitAtBedrock();
        mapGenerator.inPit = OmsRasterReader.readRaster(inPit_filepath);
        mapGenerator.process();

        GridCoverage2D outPsiInitAtBedrock2 = mapGenerator.outPsiInitAtBedrock;
        OmsRasterWriter.writeRaster(outPsiInitAtBedrock_filepath2, outPsiInitAtBedrock2);

        // Test 4 -> MUST produce a map based on passed Psi value
        mapGenerator = new OmsPsiInitAtBedrock();
        mapGenerator.inPit = OmsRasterReader.readRaster(inPit_filepath);
        mapGenerator.pPsiInitAtBedrockConstant = mockPsiValue;
        mapGenerator.process();

        GridCoverage2D outPsiInitAtBedrock3 = mapGenerator.outPsiInitAtBedrock;
        OmsRasterWriter.writeRaster(outPsiInitAtBedrock_filepath3, outPsiInitAtBedrock3);

    }


    public void _test_CalculateInitialSoilMoisture_V0() throws Exception {

        double soilThickness = 0.2347537;
        double theta_s = 0.23;
        double theta_r = 0.0355;
        double psi_i = 0.05;
        double alfaVanGenuchten = 2.98;
        double nVanGenuchten = 1.2616;

        double mV0 = OmsV0.calculateNodeInitialSoilMoisture_V0(soilThickness, theta_s, theta_r, psi_i, alfaVanGenuchten, nVanGenuchten);

        assertEquals(0.03381604, mV0, 0.00000001);

    }

    
    public void _test_V0MapGenerator() throws Exception {

        pm.message("");
        pm.message("########################################");
        pm.message("### Test test_V0MapGenerator STARTED ###");
        pm.message("########################################");

        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        String inTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String inTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String inPsiInitAtBedrock_filepath = outputFolder + "basin_PsiInitAtBedrock_const-based.asc";
        String inAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String inNVanGen_filepath = outputFolder + "basin_n.asc";

        String outV0_filepath = outputFolder + "basin_V0.asc";

        OmsV0 mapGenerator = new OmsV0();

        mapGenerator.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        mapGenerator.inTheta_s = OmsRasterReader.readRaster(inTheta_s_filepath);
        mapGenerator.inTheta_r = OmsRasterReader.readRaster(inTheta_r_filepath);
        mapGenerator.inPsiInitAtBedrock = OmsRasterReader.readRaster(inPsiInitAtBedrock_filepath);
        mapGenerator.inAlfaVanGen = OmsRasterReader.readRaster(inAlfaVanGen_filepath);
        mapGenerator.inNVanGen = OmsRasterReader.readRaster(inNVanGen_filepath);

        mapGenerator.process();

        GridCoverage2D outV0 = mapGenerator.outV0;
        OmsRasterWriter.writeRaster(outV0_filepath, outV0);
    }
    
   
    @Test
    public void _test_OmsCislam_30y_1h3h() throws Exception {
        
        pm.message("");
        pm.message("#############################################");
        pm.message("### Test test_OmsCislam 30y-1h,3h STARTED ###");
        pm.message("#############################################");
        
        OmsCislam cislam = new OmsCislam();
        
        cislam.pOutFolder = outputFolder;
        
        cislam.doSaveByProducts = true;
        cislam.doCalculateInitialSafetyFactor = true;
        
        cislam.pReturnTimes = "30";
        cislam.pRainfallDurations = "1, 3";

        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String inFlow_filepath = inputFolder + "basin_flowdirections.asc";
        String inSlopeZeroFixed_filepath = outputFolder + "basin_slope_zero_and_border_fixed.asc";
        String inAb_filepath = inputFolder + "basin_ab.asc";

        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        String inPsiInitAtBedrock_filepath = outputFolder + "basin_PsiInitAtBedrock_const-based.asc";
        String inTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String inTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String inAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String inNVanGen_filepath = outputFolder + "basin_n.asc";
        String inKsat_filepath = outputFolder + "basin_ksat.asc";
        String inGammaSoil_filepath = outputFolder + "basin_gamma_soil.asc";
        String inCohesion_filepath = outputFolder + "basin_cohesion.asc";
        String inPhi_filepath = outputFolder + "basin_phi.asc";

        cislam.inPit = OmsRasterReader.readRaster(inPit_filepath);
        cislam.inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        cislam.inSlope = OmsRasterReader.readRaster(inSlopeZeroFixed_filepath);
        cislam.inAb = OmsRasterReader.readRaster(inAb_filepath);
        cislam.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        cislam.inTheta_s = OmsRasterReader.readRaster(inTheta_s_filepath);
        cislam.inTheta_r = OmsRasterReader.readRaster(inTheta_r_filepath);
        cislam.inPsiInitAtBedrock = OmsRasterReader.readRaster(inPsiInitAtBedrock_filepath);
        
        cislam.pSigma1 = ParameterCalculationFunctions.pSigma1;
        cislam.pmF = ParameterCalculationFunctions.pmF;
        cislam.pCV = ParameterCalculationFunctions.pCV;
        
        cislam.inAlfaVanGen = OmsRasterReader.readRaster(inAlfaVanGen_filepath);
        cislam.inNVanGen = OmsRasterReader.readRaster(inNVanGen_filepath);
        cislam.inKsat = OmsRasterReader.readRaster(inKsat_filepath);
        cislam.inGammaSoil = OmsRasterReader.readRaster(inGammaSoil_filepath);
        cislam.inCohesion = OmsRasterReader.readRaster(inCohesion_filepath);
        cislam.inPhi = OmsRasterReader.readRaster(inPhi_filepath);

        cislam.process();
        
        // CUMULATED PARAMETERS
        assertEquals(1.56312629,
                RandomIterFactory.create(cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_RetTime_TOTAL_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.00001);
        assertEquals(1.49518389,
                RandomIterFactory.create(cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_RetTime_TOTAL_Raster.getRenderedImage(),null).getSampleDouble(251, 683, 0),
                0.00001);
        assertEquals(1.71280162,
                RandomIterFactory.create(cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_RetTime_TOTAL_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.00001);
        assertEquals(1.54617027,
                RandomIterFactory.create(cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_RetTime_TOTAL_Raster.getRenderedImage(),null).getSampleDouble(253, 683, 0),
                0.00001);        
        
    }
    
    @Test
    public void _test_OmsCislam_30y_1h3h6h12h24h() throws Exception {
        
        pm.message("");
        pm.message("########################################################");
        pm.message("### Test test_OmsCislam 30y-1h,3h,6h,12h,24h STARTED ###");
        pm.message("########################################################");
        
        OmsCislam cislam = new OmsCislam();
        
        cislam.pOutFolder = outputFolder;
        
        cislam.doSaveByProducts = false;
        cislam.doCalculateInitialSafetyFactor = true;
        
        cislam.pReturnTimes = "30";
        cislam.pRainfallDurations = "1, 3, 6, 12, 24";

        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String inFlow_filepath = inputFolder + "basin_flowdirections.asc";
        String inSlopeZeroFixed_filepath = outputFolder + "basin_slope_zero_and_border_fixed.asc";
        String inAb_filepath = inputFolder + "basin_ab.asc";

        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        String inPsiInitAtBedrock_filepath = outputFolder + "basin_PsiInitAtBedrock_const-based.asc";
        String inTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String inTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String inAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String inNVanGen_filepath = outputFolder + "basin_n.asc";
        String inKsat_filepath = outputFolder + "basin_ksat.asc";
        String inGammaSoil_filepath = outputFolder + "basin_gamma_soil.asc";
        String inCohesion_filepath = outputFolder + "basin_cohesion.asc";
        String inPhi_filepath = outputFolder + "basin_phi.asc";

        cislam.inPit = OmsRasterReader.readRaster(inPit_filepath);
        cislam.inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        cislam.inSlope = OmsRasterReader.readRaster(inSlopeZeroFixed_filepath);
        cislam.inAb = OmsRasterReader.readRaster(inAb_filepath);
        cislam.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        cislam.inTheta_s = OmsRasterReader.readRaster(inTheta_s_filepath);
        cislam.inTheta_r = OmsRasterReader.readRaster(inTheta_r_filepath);
        cislam.inPsiInitAtBedrock = OmsRasterReader.readRaster(inPsiInitAtBedrock_filepath);
        
        cislam.pSigma1 = ParameterCalculationFunctions.pSigma1;
        cislam.pmF = ParameterCalculationFunctions.pmF;
        cislam.pCV = ParameterCalculationFunctions.pCV;
        
        cislam.inAlfaVanGen = OmsRasterReader.readRaster(inAlfaVanGen_filepath);
        cislam.inNVanGen = OmsRasterReader.readRaster(inNVanGen_filepath);
        cislam.inKsat = OmsRasterReader.readRaster(inKsat_filepath);
        cislam.inGammaSoil = OmsRasterReader.readRaster(inGammaSoil_filepath);
        cislam.inCohesion = OmsRasterReader.readRaster(inCohesion_filepath);
        cislam.inPhi = OmsRasterReader.readRaster(inPhi_filepath);

        cislam.process();
    }

    @Test
    public void _test_OmsCislam_10y_24h() throws Exception {
        
        pm.message("");
        pm.message("###########################################");
        pm.message("### Test test_OmsCislam 10y-24h STARTED ###");
        pm.message("###########################################");        

        OmsCislam cislam = new OmsCislam();
        
        cislam.pOutFolder = outputFolder;
        
        cislam.doSaveByProducts = true;
        
        cislam.pReturnTimes = "10";
        cislam.pRainfallDurations = "24";

        String inPit_filepath = inputFolder + "basin_depitted_dem.asc";
        String inFlow_filepath = inputFolder + "basin_flowdirections.asc";
        String inSlopeZeroFixed_filepath = outputFolder + "basin_slope_zero_and_border_fixed.asc";
        String inAb_filepath = inputFolder + "basin_ab.asc";

        String inSoilThickness_filepath = outputFolder + "basin_soilthickness.asc";
        String inPsiInitAtBedrock_filepath = outputFolder + "basin_PsiInitAtBedrock_const-based.asc";
        String inTheta_s_filepath = outputFolder + "basin_thetas.asc";
        String inTheta_r_filepath = outputFolder + "basin_thetar.asc";
        String inAlfaVanGen_filepath = outputFolder + "basin_alfa.asc";
        String inNVanGen_filepath = outputFolder + "basin_n.asc";
        String inKsat_filepath = outputFolder + "basin_ksat.asc";
        String inGammaSoil_filepath = outputFolder + "basin_gamma_soil.asc";
        String inCohesion_filepath = outputFolder + "basin_cohesion.asc";
        String inPhi_filepath = outputFolder + "basin_phi.asc";

        cislam.inPit = OmsRasterReader.readRaster(inPit_filepath);
        cislam.inFlow = OmsRasterReader.readRaster(inFlow_filepath);
        cislam.inSlope = OmsRasterReader.readRaster(inSlopeZeroFixed_filepath);
        cislam.inAb = OmsRasterReader.readRaster(inAb_filepath);
        cislam.inSoilThickness = OmsRasterReader.readRaster(inSoilThickness_filepath);
        cislam.inTheta_s = OmsRasterReader.readRaster(inTheta_s_filepath);
        cislam.inTheta_r = OmsRasterReader.readRaster(inTheta_r_filepath);
        cislam.inPsiInitAtBedrock = OmsRasterReader.readRaster(inPsiInitAtBedrock_filepath);
        cislam.inAlfaVanGen = OmsRasterReader.readRaster(inAlfaVanGen_filepath);
        cislam.inNVanGen = OmsRasterReader.readRaster(inNVanGen_filepath);
        cislam.inKsat = OmsRasterReader.readRaster(inKsat_filepath);
        cislam.inGammaSoil = OmsRasterReader.readRaster(inGammaSoil_filepath);
        cislam.inCohesion = OmsRasterReader.readRaster(inCohesion_filepath);
        cislam.inPhi = OmsRasterReader.readRaster(inPhi_filepath);

        cislam.pSigma1 = ParameterCalculationFunctions.pSigma1;
        cislam.pmF = ParameterCalculationFunctions.pmF;
        cislam.pCV = ParameterCalculationFunctions.pCV;

        cislam.process();
        
        // #############################################################################        
        // All these test work with Return Time = 10years & Rainfall Duration=24h;
        
        // Psi Equation (9) due to vertical infiltration
        double[] pixel = new double[cislam.out_psi_b_Eq9_Raster.getNumSampleDimensions()];
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq9_Raster.getRenderedImage(),null).getPixel(0, 0, pixel);  
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(Double.NaN, pixel[1], 0.000001); // After 2 hour of rain
        assertEquals(Double.NaN, pixel[2], 0.000001); // After 3 hour of rain
        assertEquals(Double.NaN, pixel[3], 0.000001); // After 4 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq9_Raster.getRenderedImage(),null).getPixel(252, 682, pixel);  
        assertEquals(0.0163056380, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(Double.NaN, pixel[1], 0.000001); // After 2 hour of rain
        assertEquals(Double.NaN, pixel[2], 0.000001); // After 3 hour of rain
        assertEquals(Double.NaN, pixel[3], 0.000001); // After 4 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq9_Raster.getRenderedImage(),null).getPixel(251, 684, pixel);
        assertEquals(0.024350437, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(0.0007027707, pixel[1], 0.000001); // After 2 hours of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq9_Raster.getRenderedImage(),null).getPixel(253, 683, pixel);
        assertEquals(0.02519662914, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(0.0023021080, pixel[1], 0.000001); // After 2 hours of rain
        
        
        // CUMULATED PARAMETERS
        assertEquals(184870.39683,
                RandomIterFactory.create(cislam.out_cumRatio_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.00001);
        assertEquals(4.0,
                RandomIterFactory.create(cislam.out_cumDist_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.000001);
        assertEquals(5.65685424,
                RandomIterFactory.create(cislam.out_cumDist_Raster.getRenderedImage(),null).getSampleDouble(249, 685, 0),
                0.000001);
        assertEquals(1,
                RandomIterFactory.create(cislam.out_cumCont_Raster.getRenderedImage(),null).getSample(252, 683, 0));        
        assertEquals(10,
                RandomIterFactory.create(cislam.out_cumCont_Raster.getRenderedImage(),null).getSample(254, 692, 0));
        
        // AVERAGED CUMULATED PAPAMETERS
        assertEquals(0.907348,
                RandomIterFactory.create(cislam.out_avg_slope_cum_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.000001);        
        assertEquals(0.23475366,
                RandomIterFactory.create(cislam.out_avg_soil_thickness_cum_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.000001);        
        assertEquals(1.5186161,
                RandomIterFactory.create(cislam.out_avg_th_time_cum_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.000001);
        assertEquals(2.1052631,
                RandomIterFactory.create(cislam.out_avg_ab_cum_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.000001);

        // BORDER-FIXED AVERAGED and NOT CUMULATED PARAMETERS
        assertEquals(184870.396834,
                RandomIterFactory.create(cislam.out_cumRatio_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.00001);
        assertEquals(4.0,
                RandomIterFactory.create(cislam.out_cumDist_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.000001);
        assertEquals(0.907348,
                RandomIterFactory.create(cislam.out_avg_slope_cum_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.000001);        
        assertEquals(0.23475366,
                RandomIterFactory.create(cislam.out_avg_soil_thickness_cum_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.000001);        
        assertEquals(1.5186161,
                RandomIterFactory.create(cislam.out_avg_th_time_cum_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.000001);
        assertEquals(16,
                RandomIterFactory.create(cislam.out_avg_ab_cum_fixed_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.000001);
        
        // CHECKS on the Dinamic Linear Topographic Index
        pixel = new double[cislam.out_IT_din_lin_Raster.getNumSampleDimensions()];
        
        pixel = RandomIterFactory.create(cislam.out_IT_din_lin_Raster.getRenderedImage(),null).getPixel(0, 0, pixel);  
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(Double.NaN, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(Double.NaN, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(Double.NaN, pixel[3], 0.000001); // After 4 hours of rain        
        
        pixel = RandomIterFactory.create(cislam.out_IT_din_lin_Raster.getRenderedImage(),null).getPixel(249, 684, pixel);  
        assertEquals(0.012814788, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(0.080519109, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(0.148223431, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(0.215927752, pixel[3], 0.000001); // After 4 hours of rain
        
        pixel = RandomIterFactory.create(cislam.out_IT_din_lin_Raster.getRenderedImage(),null).getPixel(252, 682, pixel);   
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(0.021749980, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(1.015758276, pixel[23], 0.000001); // After 24 hours of rain
        
        pixel = RandomIterFactory.create(cislam.out_IT_din_lin_Raster.getRenderedImage(),null).getPixel(252, 683, pixel);     
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(0.055083983, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(0.169512376, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(0.283940769, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(2.57250863, pixel[23], 0.000001); // After 24 hours of rain
        
        pixel = new double[cislam.out_psi_b_Eq12_Raster.getNumSampleDimensions()];
        // CHECKS on the Psi at bedrock in positive-pressure areas
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq12_Raster.getRenderedImage(),null).getPixel(0, 0, pixel);  
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(Double.NaN, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(Double.NaN, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(Double.NaN, pixel[3], 0.000001); // After 4 hours of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq12_Raster.getRenderedImage(),null).getPixel(249, 684, pixel); 
        assertEquals(-0.001267909, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.007966649, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.014665390, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.021364130, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.1, pixel[23], 0.000001); // After 24 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq12_Raster.getRenderedImage(),null).getPixel(250, 684, pixel); 
        assertEquals(-0.0003667940, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.0023046753, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.0042425567, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.0061804381, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.044938065, pixel[23], 0.000001); // After 24 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Eq12_Raster.getRenderedImage(),null).getPixel(252, 682, pixel);     
        assertEquals(Double.NaN, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.002151967, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.006622343, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.011092719, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.100500248, pixel[23], 0.000001); // After 24 hour of rain
        
        // CHECKS on the Combined Psi at bedrock maps resulting from combination of maps from Eq.(9) and Eq.(12)
        pixel = new double[cislam.out_psi_b_Combined_Raster.getNumSampleDimensions()];
        pixel = RandomIterFactory.create(cislam.out_psi_b_Combined_Raster.getRenderedImage(),null).getPixel(252, 682, pixel);     
        assertEquals(0.0163056380, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.0021519671, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.0066223435, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.011092719, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.100500248, pixel[23], 0.000001); // After 24 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Combined_Raster.getRenderedImage(),null).getPixel(249, 684, pixel); 
        assertEquals(-0.001267909, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.007966649, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.014665390, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.021364130, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.1, pixel[23], 0.000001); // After 24 hour of rain
        
        pixel = RandomIterFactory.create(cislam.out_psi_b_Combined_Raster.getRenderedImage(),null).getPixel(250, 684, pixel); 
        assertEquals(-0.0003667940, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(-0.0023046753, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(-0.0042425567, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(-0.0061804381, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(-0.044938065, pixel[23], 0.000001); // After 24 hour of rain
         
        
        // CHECKS on the Safety Factor computed for initial conditions (geomecanic and hydrologic)
        assertEquals(1.7990060,
                RandomIterFactory.create(cislam.out_SafetyFactor_Initial_Raster.getRenderedImage(),null).getSampleDouble(252, 682, 0),
                0.00001);
        assertEquals(1.92929021,
                RandomIterFactory.create(cislam.out_SafetyFactor_Initial_Raster.getRenderedImage(),null).getSampleDouble(251, 683, 0),
                0.00001);
        assertEquals(2.32487913,
                RandomIterFactory.create(cislam.out_SafetyFactor_Initial_Raster.getRenderedImage(),null).getSampleDouble(252, 683, 0),
                0.00001);
        assertEquals(1.66490920,
                RandomIterFactory.create(cislam.out_SafetyFactor_Initial_Raster.getRenderedImage(),null).getSampleDouble(253, 683, 0),
                0.00001);
        assertEquals(1.58080629,
                RandomIterFactory.create(cislam.out_SafetyFactor_Initial_Raster.getRenderedImage(),null).getSampleDouble(252, 684, 0),
                0.00001);
        
        // #############################################################################
        // CHECKS on the HYDROLOGICALLY-CONTROLLED SAFETY FACTOR
        pixel = new double[cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_Raster.getNumSampleDimensions()];
        pixel = RandomIterFactory.create(cislam.out_SafetyFactor_InfiniteSlope_accounting_for_SaturatedZones_Raster.getRenderedImage(),null).getPixel(252, 682, pixel); 
        assertEquals(1.715243651, pixel[0], 0.000001); // After 1 hour of rain
        assertEquals(1.660170896, pixel[1], 0.000001); // After 2 hours of rain
        assertEquals(1.6463652694, pixel[2], 0.000001); // After 3 hours of rain
        assertEquals(1.632559642, pixel[3], 0.000001); // After 4 hours of rain
        assertEquals(1.356447111, pixel[23], 0.000001); // After 24 hour of rain
        
        // All the ABOVE test work with Return Time = 10years & Rainfall Duration=24h on CISLAM Dataset;
        // #############################################################################
        
    }
    
    @Test
    public void _test_OmsSafetyFactorsWorstCaseComposer_30y_1h3h() throws Exception {
        
        pm.message("");
        pm.message("####################################################################");
        pm.message("### Test test_OmsSafetyFactorsWorstCaseComposer_30y_1h3h STARTED ###");
        pm.message("####################################################################");
        
        OmsSafetyFactorsWorstCaseComposer model = new OmsSafetyFactorsWorstCaseComposer();
        
        model.pReturnTime = 30;

        List<GridCoverage2D> gridList = new ArrayList<GridCoverage2D>();
        
        String inMap1_filepath = outputFolder + "Safety_Factor_Hydrologic_30y_1h.asc";
        String inMap2_filepath = outputFolder + "Safety_Factor_Hydrologic_30y_3h.asc";

        String outMap_filepath = outputFolder + "SAFETY_FACTOR_HYDROLOGIC_TEST_30y_1h3h_TOT.asc";
        
        gridList.add(OmsRasterReader.readRaster(inMap1_filepath));
        gridList.add(OmsRasterReader.readRaster(inMap2_filepath));
        
        model.inRasters = gridList;
        
        model.process();
        
        GridCoverage2D outTotal = model.outSafetyFactorTotal;
        OmsRasterWriter.writeRaster(outMap_filepath, outTotal);
    }
}
