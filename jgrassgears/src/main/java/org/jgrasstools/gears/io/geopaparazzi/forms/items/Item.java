package org.jgrasstools.gears.io.geopaparazzi.forms.items;

public interface Item {
    String getKey();
    void setValue(String value);
    String getValue();
}
